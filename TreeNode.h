#pragma once
#include "ChessBoark_State.h"
#include <vector>
using namespace std;

class TreeNode
{
public:
	TreeNode(ChessBoark_State state,int iteration);
	TreeNode(ChessBoark_State state, TreeNode* parent,int iteration);
	//TreeNode();
	~TreeNode();
	void Selection();//选择
	void expansion();//拓展
	void Backpropagation();//回溯(反向传播)
	void Simluation();//模拟
	int children_count();//统计可拓展子节点个数
private:
	//int my_chess;                 //我所执的棋子颜色
	//int this_round_chess;         //本回合需要落子的颜色
	int iterations;            //迭代次数
	ChessBoark_State new_state; //本轮的新棋盘
	int isTerminal;             //是否终局
	int Selection_num;     //选择次数
	bool isFullyExpanded;       //是否完全拓展
	TreeNode* my_parent;           //父节点
	vector<TreeNode*>children;   //子节点
	int numVisits;              //节点访问次数
	int totalReward;            //模拟胜负积分
	int children_num;           //可拓展子节点总数
	double ucb;                 //节点探索价值
	int expansion_i;
	int expansion_j;


};

