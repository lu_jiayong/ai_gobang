#pragma once
#define _AFXDLL//为了方便是用mfc类
#include <afxwin.h>
#include <iostream>
#include <stdio.h>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

using namespace std;
using namespace cv;

class GoBang_OpenCV
{
public:
	GoBang_OpenCV();//构造函数
	~GoBang_OpenCV();//虚构函数
	void initGDI();//初始化GDI截图
	void gdiScreenCapture();//GDI截图并转换
	Mat get_new_img();//读取一帧截图
	void free_img();//释放截图动态内存
	void using_mouse(int x, int y, int title_x, int title_y);//模拟鼠标

private:
	LPVOID screenCaptureData = NULL;
	HBITMAP hBitmap;
	HDC hDDC;
	HDC hCDC;
	int nWidth;
	int nHeight;
	Mat img;
	IplImage* img_ipl;
	IplImage* img2_ipl;
};

