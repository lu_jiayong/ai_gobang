#pragma once

#define no_chess -1
#define white_chess 1
#define black_chess 0

extern int my_chess;

struct chessboark {
	int chess = -1;
};

struct chess_id
{
	int x;
	int y;
	int chess_color;
};
