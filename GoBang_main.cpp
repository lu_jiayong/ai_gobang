#include"GoBang_OpenCV.h"
#include <cstdlib>
#include <ctime>
#include "TreeNode.h"

int my_chess;

int main()
{
	int s = 0;
	char key = 0, color[2][16] = { "黑色","白色" };
	srand((int)time(0));  // 产生随机种子  把0换成NULL也行
	GoBang_OpenCV GoBang_CV;
	Mat img,
		title_match_result,
		chessboark_ROI,
		chessboark_roi_gray,
		action_ROI, action_match_result,
		chessboark_roi_thres,
		title_img,
		action_img;
	title_img = imread("image/title.jpg");
	action_img = imread("image/action.jpg");
	cout << "0为黑棋\n"
		"1为白棋\n"
		"请输入您所执的棋子";
	cin >> my_chess;

	//窗口定位
	int title_x, title_y,
		action_x, action_y;
	double title_minVal, title_maxVal,
		action_minVal, action_maxVal;
	Point title_minLoc, title_maxLoc,
		action_minLoc, action_maxLoc;
	title_x = title_y = action_x = action_y = 0;

	img = GoBang_CV.get_new_img();
	matchTemplate(img, title_img, title_match_result, TM_CCOEFF_NORMED);
	minMaxLoc(title_match_result, &title_minVal, &title_maxVal, &title_minLoc, &title_maxLoc);
	printf("模板匹配定位棋盘标题；最小结果：%f,最大结果：%f\n",
		title_minVal, title_maxVal);
	if (title_maxVal <= 0.9) {
		printf("未找到棋盘标题，请重试。");
		return 0;
	}
	//
	title_x = title_maxLoc.x + 30;
	title_y = title_maxLoc.y + 60;
	Rect chessboark_rect(title_x, title_y, 850, 850);//左上角x,y,长，宽
	Rect action_rect(title_x + 850, title_y + 800, 150, 50);
	//

	while (1)
	{
		ChessBoark_State sss;
		img = GoBang_CV.get_new_img();
		chessboark_ROI = img(chessboark_rect);
		action_ROI = img(action_rect);
		matchTemplate(action_ROI, action_img, action_match_result, TM_CCOEFF_NORMED);
		minMaxLoc(action_match_result, &action_minVal, &action_maxVal, &action_minLoc, &action_maxLoc);
		if (action_maxVal <= 0.7) {
			printf("未找到action!匹配度:%f\n我的棋子%d\n", action_maxVal, my_chess);
		}
		else {
			action_ROI = img(action_rect);
			cvtColor(chessboark_ROI, chessboark_roi_gray, CV_BGR2GRAY);
			vector<Vec3f> circles;
			HoughCircles(chessboark_roi_gray, circles, HOUGH_GRADIENT, 1, 50, 5, 10, 18, 30);
			//chessboark_ai mcts;
			for (unsigned int i = 0; i < circles.size(); i++) {
				circle(chessboark_ROI, Point(circles[i][0], circles[i][1]), circles[i][2], Scalar(255, 0, 255), 2);
				int chess_x = int(circles[i][0] / 57);
				int chess_y = int(circles[i][1] / 57);
				int col = int(chessboark_roi_gray(Rect(int(circles[i][0]), int(circles[i][1]), 1, 1)).data[0]);
				int col_id;
				if (col == 0) {
					col_id = 0;
				}
				else {
					col_id = 1;
				}
				int screen_x = circles[i][0] + title_x;
				int screen_y = circles[i][1] + title_y;
				printf("第%d个棋子  颜色: %s\n"
					"ROI相对像素坐标: %d,%d, 1080P屏幕绝对像素坐标: %d,%d, 15*15棋盘转换坐标: %d,%d \n",
					i + 1, color[col_id], int(circles[i][0]), int(circles[i][1]),
					int(screen_x), int(screen_y), chess_x, chess_y);
				sss.set_chess_data(chess_y, chess_x, col_id);
				//mcts.set_chessboark(chess_y, chess_x, col_id);
			}
			int x1, y1;
			x1 = (rand() % 15);
			y1 = (rand() % 15);
			int x = x1 * 57 + title_x + 23;
			int y = y1 * 57 + title_y + 23;
			GoBang_CV.using_mouse(x, y, title_x, title_y);
			int pp=sss.GoBang_judge();
			printf("胜利方%d\n我的棋子%d\n", pp, sss.get_mychess());
			s++;
			printf("本次为第%d回合；本次下子  %d ，%d\n", s, x1, y1);
			imshow("chessboark_ROI", chessboark_ROI);
			key = waitKey(3000);
			if (key == 27) {
				break;
			}
		}
		GoBang_CV.free_img();
		key = waitKey(16);
		if (key == 27) {
			break;
		}
	}

}