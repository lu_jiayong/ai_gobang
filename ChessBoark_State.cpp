#include "ChessBoark_State.h"

ChessBoark_State::ChessBoark_State(chessboark state[15][15])
{
    memcpy(this->state, state, sizeof(chessboark) * 15 * 15);
}
  
//ChessBoark_State::ChessBoark_State()
//{
//}

ChessBoark_State::~ChessBoark_State()
{
}

void ChessBoark_State::set_chess_data(int x, int y, int data)
{
    state[x][y].chess = data;
}

void ChessBoark_State::set_this_round_chess(int x, int y, int data)
{
    this_round_chess.x = x;
    this_round_chess.y = y;
    this_round_chess.chess_color = data;
}

int ChessBoark_State::GoBang_judge()
{
	int result = no_chess;
	//黑色
	for (int i = 0; i < 15; i++) {
		for (int j = 0; j < 15; j++) {
			if (state[i][j].chess == black_chess) {
				int result_count = 0;
				//检测横联5个
				for (int k = 1; k < 5; k++) {
					if (j <= 10) {
						if (state[i][j + k].chess == black_chess) {
							result_count++;
						}
					}
				}
				if (result_count >= 4) {
					result = black_chess;
					return result;
				}
				else {
					result_count = 0;
				}
				//检测竖联5个
				for (int k = 1; k < 5; k++) {
					if (i <= 10) {
						if (state[i + k][j].chess == black_chess) {
							result_count++;
						}
					}
				}
				if (result_count >= 4) {
					result = black_chess;
					return result;
				}
				else {
					result_count = 0;
				}
				//检测正斜联5个 
				for (int k = 1; k < 5; k++) {
					if (i <= 10 && j <= 10) {
						if (state[i + k][j + k].chess == black_chess) {
							result_count++;
						}
					}
				}
				if (result_count >= 4) {
					result = black_chess;
					return result;
				}
				else {
					result_count = 0;
				}
				//检测反斜联5个
				for (int k = 1; k < 5; k++) {
					if (i <= 10 && j >= 4) {
						if (state[i + k][j - k].chess == black_chess) {
							result_count++;
						}
					}

				}
				if (result_count >= 4) {
					result = black_chess;
					return result;
				}
				else {
					result_count = 0;
				}
			}
		}

	}
	//白色
	for (int i = 0; i < 15; i++) {
		for (int j = 0; j < 15; j++) {
			if (state[i][j].chess == white_chess) {
				int result_count = 0;
				//检测横联5个
				for (int k = 1; k < 5; k++) {
					if (j <= 10) {
						if (state[i][j + k].chess == white_chess) {
							result_count++;
						}
					}
				}
				if (result_count >= 4) {
					result = white_chess;
					return result;
				}
				else {
					result_count = 0;
				}
				//检测竖联5个
				for (int k = 1; k < 5; k++) {
					if (i <= 10) {
						if (state[i + k][j].chess == white_chess) {
							result_count++;
						}
					}
				}
				if (result_count >= 4) {
					result = white_chess;
					return result;
				}
				else {
					result_count = 0;
				}
				//检测正斜联5个 
				for (int k = 1; k < 5; k++) {
					if (i <= 10 && j <= 10) {
						if (state[i + k][j + k].chess == white_chess) {
							result_count++;
						}
					}
				}
				if (result_count >= 4) {
					result = white_chess;
					return result;
				}
				else {
					result_count = 0;
				}
				//检测反斜联5个
				for (int k = 1; k < 5; k++) {
					if (i <= 10 && j >= 4) {
						if (state[i + k][j - k].chess == white_chess) {
							result_count++;
						}
					}

				}
				if (result_count >= 4) {
					result = white_chess;
					return result;
				}
				else {
					result_count = 0;
				}
			}
		}

	}
	return result;
}

int ChessBoark_State::get_mychess()
{
	//int data = my_chess;
	return my_chess;
}
