#include "TreeNode.h"

TreeNode::TreeNode(ChessBoark_State state,int iteration)
{
    this->new_state = state;
    this->my_parent = NULL;
	this->iterations = iteration;
	numVisits = 0;
	totalReward = 0;
	expansion_i = 0;
	expansion_j = 0;
	children_num = children_count();
	isTerminal = new_state.GoBang_judge();
}

TreeNode::TreeNode(ChessBoark_State state, TreeNode* parent, int iteration)
{
    this->new_state = state;
    this->my_parent = parent;
	this->iterations = iteration;
	numVisits = 0;
	totalReward = 0;
	expansion_i = 0;
	expansion_j = 0;
	children_num = children_count();
	isTerminal = new_state.GoBang_judge();
}

//TreeNode::TreeNode()
//{
//
//}

TreeNode::~TreeNode()
{
}

void TreeNode::Selection()//选择
{   //拓展
	if (children.size() <= children_num) {
		expansion();
	}
	//未完全拓展选择
	if (Selection_num <= children_num) {
		children[Selection_num]->Simluation();
	}
}

void TreeNode::expansion()//拓展
{
	for (expansion_i; expansion_i < 15; expansion_i++) {
		for (expansion_j; expansion_j < 15; expansion_j++) {
			
			if (this->new_state.state[expansion_i][expansion_j].chess == -1) {
				TreeNode my_children(this->new_state, this, this->iterations);
				my_children.new_state.set_chess_data(expansion_i, expansion_j, 1);//->new_state.set_chess_data(i, j, 1);
				my_children.new_state.set_this_round_chess(expansion_i, expansion_j, 1);
				children.push_back(&my_children);
				break;
			}
		}
	}
}

void TreeNode::Backpropagation()//回溯(反向传播)
{
}

void TreeNode::Simluation()//模拟
{
	ChessBoark_State Simluation_state = new_state;
	int chess=my_chess;
	while (true)
	{
		if (chess == my_chess) {

		}
	}
}

int TreeNode::children_count()//统计可拓展子节点个数
{
	int count = 0;
	for (int i = 0; i < 15; i++) {
		for (int j = 0; j < 15; j++) {
			if (new_state.state[i][j].chess == -1) {
				count++;
			}
		}
	}
	return count;
}
