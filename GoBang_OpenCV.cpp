#include "GoBang_OpenCV.h"

/*构造函数*/
GoBang_OpenCV::GoBang_OpenCV()
{
    this->initGDI();
}

/*虚构函数*/
GoBang_OpenCV::~GoBang_OpenCV()
{
}

/*初始化GDI截图*/
void GoBang_OpenCV::initGDI()
{
	nWidth = GetSystemMetrics(SM_CXSCREEN);//得到屏幕的分辨率的x
	nHeight = GetSystemMetrics(SM_CYSCREEN);//得到屏幕分辨率的y
	int cout = nWidth * nHeight * 4;
	screenCaptureData = new char[cout];
	memset(screenCaptureData, 0, nWidth);
	// Get desktop DC, create a compatible dc, create a comaptible bitmap and select into compatible dc.
	hDDC = GetDC(GetDesktopWindow());//得到屏幕的dc
	hCDC = CreateCompatibleDC(hDDC);//
	hBitmap = CreateCompatibleBitmap(hDDC, nWidth, nHeight);//得到位图
	SelectObject(hCDC, hBitmap); //好像总得这么写。
}

/*GDI截图并转换*/
void GoBang_OpenCV::gdiScreenCapture()
{
	BitBlt(hCDC, 0, 0, nWidth, nHeight, hDDC, 0, 0, SRCCOPY);
	int cout = nWidth * nHeight * 4;
	GetBitmapBits(hBitmap, cout, screenCaptureData);//得到位图的数据，并存到screenCaptureData数组中。
	img_ipl = cvCreateImage(cvSize(nWidth, nHeight), 8, 4);//创建一个rgba格式的IplImage
	memcpy(img_ipl->imageData, screenCaptureData, cout);//这样比较浪费时间，但写的方便。这里必须得是*4，上面的链接写的是*3，这是不对的。
	img2_ipl = cvCreateImage(cvSize(nWidth, nHeight), 8, 3);//创建一个bgr格式的IplImage，可以没有这个Img2这个变量。
	cvCvtColor(img_ipl, img2_ipl, CV_BGRA2BGR);
	img = cvarrToMat(img2_ipl);
}

/*读取一帧截图*/
Mat GoBang_OpenCV::get_new_img()
{
	this->gdiScreenCapture();
	return img;
}

/*释放截图动态内存*/
void GoBang_OpenCV::free_img()
{
	cvReleaseImage(&img_ipl);
	cvReleaseImage(&img2_ipl);
}

/*模拟鼠标*/
void GoBang_OpenCV::using_mouse(int x, int y, int title_x, int title_y)
{
	//将鼠标光标移动到 指定的位置     例子中屏幕分辨率1920x1080  在鼠标坐标系统中，屏幕在水平和垂直方向上均匀分割成65535×65535个单元
	mouse_event(MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE, x * 65535 / 1920, y * 65535 / 1080, 0, 0);
	Sleep(30);//稍微延时20ms 
	mouse_event(MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0);//鼠标左键按下 
	mouse_event(MOUSEEVENTF_LEFTUP, 0, 0, 0, 0);//鼠标左键抬起
}
