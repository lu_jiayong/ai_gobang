#pragma once
#include "ChessBoark.h"
#include <string.h>


class ChessBoark_State
{
public:
	ChessBoark_State(chessboark state[15][15]);
	ChessBoark_State();
	~ChessBoark_State();
	void set_chess_data(int x, int y, int data);
	void set_this_round_chess(int x, int y, int data);
	int GoBang_judge();
	chessboark state[15][15];
	int get_mychess();
	chess_id this_round_chess;

private:
	
};

